"""
Created 05 Sep 2020
Anders Wenhaug, anders.wenhaug@solutionseeker.no
"""

import sys
import logging

from abfuse import abfuse


# TODO: Add argument parser
def main(debug: bool = False):
    root_logger = logging.getLogger()

    log_handler = logging.StreamHandler(sys.stdout)
    log_handler.setLevel(logging.DEBUG)
    log_handler.setFormatter(logging.Formatter(logging.BASIC_FORMAT))
    root_logger.addHandler(log_handler)

    if len(sys.argv) < 3:
        root_logger.critical(f"Usage: {sys.argv[0]} device mountpoint")
        sys.exit(1)

    mountpoint = sys.argv[2]
    device = sys.argv[1]

    abfuse.main(mountpoint, device, "output-fake-file", debug)


if __name__ == '__main__':
    main(debug=True)
